#!/bin/bash
#SBATCH --job-name=WRFlibs
#SBATCH --output=WRFlibs%j.out
#SBATCH --error=WRFlibs%j.out
#SBATCH --ntasks=1
#SBATCH --ntasks-per-node=8
#SBATCH --time=01:00:00
#SBATCH --mail-user=<email>
#SBATCH --reservation=meteo_16

######################################################################################
# This script installs local libraries on the Altamira system necessary for WRF and WPS
# compilations. 6 libraries suppose to be installed:
#	ZLIB
#	HDF5
#	NETDF-C
#	NETCDF-FORTRAN
#	JASPER
#	LIBPNG
# The script downloads the libraries from the web and installs them in a working 
# directory. The versions are hardcoded.
# A user just need to update the download links if other library version are wnated to
# be installed. 
# Note the the downloaded files are expected to be in .tar.gz format. 
# ------------------------------------------------------------------------------------
# Optimization flags:
# '-O3 -xHost -ip':
# 	-O3 --> optimization, recommended for codes that loops involving intensive floating 
#		point calculations.
# 	-xHost --> must be used with at least -O2. Executable created with SIMD instructions 
#		based on the CPU which compiles the code.
# 	-ip --> option enables additional interprocedural optimizations for single-file 
#		compilation.
######################################################################################

# Load libraries
export INTEL="2020"  # "2013"
export OMPI="4.1.0"  # "4.0.0
if [ $INTEL == "2013" ] ; then
	echo "Compiling with Intel version 13"
	module purge
	module load INTEL/PSXE2013u1
	module load openmpi/$OMPI-Intel13_UCX
elif [  $INTEL == "2020"  ] ; then
	echo "Compiling with Intel version 20"
	module purge
	module load INTEL/2020                              	
	module load OPENMPI/$OMPI               		
	source /gpfs/res_apps/INTEL/2020/bin/compilervars.sh intel64
else
	echo "Version of the Intel compiler not defined or not installed on the Altamira system, exiting ..."
	exit 1
fi

# Setting the the compilation flags for intel compilation (defalut is gcc interpolation)
export CC=icc
export FC=ifort
export F90=ifort
export F77=ifort
export CXX=icpc
export CPP='icc -E'
export CXXCPP='icpc -E'
export CFLAGS='-O3 -xHost -ip'		
export CXXFLAGS='-O3 -xHost -ip'	
export FFLAGS='-O3 -xHost -ip'		
export NETCDF4=1						# compilation with netcdf4
export WRFIO_NCD_LARGE_FILE_SUPPORT=1	# enables dealing with large files (default in newer WRF versions, after WRFv4.0)
export J="-j 8"							# makes compilation faster, compiles in parallel.
export LANG=en_US.UTF-8					# environment variable for specifying a locale 

# Set the working and output directory paths
export WRKDIR=`pwd`
export LIBDIR=${WRKDIR}/BUILD_LIBS/
export wrflibs=${WRKDIR}/WRFLIBS

mkdir -p $LIBDIR $wrflibs

cd ${WRKDIR}

# Installing the libraries
libs="zlib hdf5 netcdf-c netcdf-fortran jasper libpng"
for libname in $libs ; do
	echo ${libname}

	# Put netcdf-c and netcdf-fortran in the same directory, important for compiling WRF
	if [ ${libname} == "netcdf-c" ] || [ ${libname} == "netcdf-fortran" ] ; then
		outdir=$LIBDIR/netcdf
	else
		outdir=$LIBDIR/$libname
	fi

	mkdir -p $outdir

	# Set the download links for each library
	case $libname in
		zlib) 		dlink="https://zlib.net/";;
		hdf5) 		dlink="https://support.hdfgroup.org/ftp/HDF5/prev-releases/hdf5-1.12/hdf5-1.12.0/src/";;
		netcdf-c) 	dlink="https://www.unidata.ucar.edu/downloads/netcdf/ftp/netcdf-c-4.7.4.tar.gz";;
		netcdf-fortran) dlink="https://www.unidata.ucar.edu/downloads/netcdf/ftp/netcdf-fortran-4.5.3.tar.gz";;
		jasper) 	dlink="repository.timesys.com/buildsources/j/jasper/jasper-1.900.1/jasper-1.900.1.tar.gz";;
		libpng) 	dlink="https://sourceforge.net/projects/libpng/files/libpng16/1.6.37/libpng-1.6.37.tar.gz";;
	esac

	# Downloading the library
	echo $dlink
	wget  $dlink$(wget -O- $dlink | egrep -o "${libname}-[0-9\.]+.tar.gz" | sort -V  | tail -1)
	tar xvfz ${libname}*.tar.gz
	cd ${libname}*

	# Installing the library
	make clean
	if [ $libname == "hdf5" ] ; then
		# --with-default-api-version=v18 --> add this flag when compiling netcdf-c-4.8x
		./configure --with-zlib=${wrflibs} --prefix=${outdir} --enable-hl --enable-shared --enable-fortran 
	elif [ $libname == "netcdf-c" ] ; then
		CPPFLAGS="-I${wrflibs}/include" LDFLAGS="-L${wrflibs}/lib" ./configure --prefix=${outdir} --disable-dap --enable-netcdf4 --enable-shared
	elif [ $libname == "netcdf-fortran" ] ; then
		CPPFLAGS=-I${outdir}/include LDFLAGS=-L${outdir}/lib ./configure --prefix=${outdir}
	else
		./configure --prefix=${outdir}
	fi
	make
	make check 2>&1 | tee ${outdir}/make_check_${libname}.log
	make install 2>&1 | tee ${outdir}/make_install_${libname}.log

	# Setting the paths for the libraries
	export PATH=$PATH:$outdir
	export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$outdir/lib
	export LIB=$LIB:$outdir/lib
	export INCLUDE=$INCLUDE:$outdir/include
	if [ $libname == "jasper" ] ; then
		export JASPERLIB=$outdir/lib
		export JASPERINC=$outdir/include
	elif [ $libname == "netcdf-fortran" ] ; then
		export NETCDF=$outdir
	fi

	cp -r ${outdir}/* ${wrflibs}/
	cd ${WRKDIR}
	rm ${WRKDIR}/*.tar.gz

done

###############################################################################
# When all is succesully installed, it is practical to create a WRF module
# that loads all the libraries necessary to succesully compile WRF and WPS
###############################################################################

# Setting the local module path
export mpath="<path_to_folder_with_modules>"

# To use the local module path
module use $mpath

# Define a name for the new module that will load all the necessary variables
wrf_module=$mpath/wrflibs_i${INTEL}_ompi${OMPI}_test

# Create the module
cat > $wrf_module <<EOF
#%Module#######################################################################
##
## null modulefile
##
proc ModulesHelp { } {
        puts stderr "   This module configures WRF libraries compiled with compiled with INTEL/$INTEL and openmpi $OMPI"
}

module-whatis   "wrf libraries compiled with INTEL/$INTEL & OPENMPI $OMPI"

# Enviromental variables
setenv NETCDF    $wrflibs
setenv HDF5      $wrflibs
setenv JASPER    $wrflibs
setenv JASPERLIB $wrflibs/lib
setenv JASPERINC $wrflibs/include

# Define Paths
prepend-path PATH               $wrflibs/bin
prepend-path LD_LIBRARY_PATH    $wrflibs/lib
prepend-path LIB                $wrflibs/lib
prepend-path INCLUDE            $wrflibs/include
EOF

# To load newly created $wrf_module
module load $wrf_module
